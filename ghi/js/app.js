function createCard(name, description, pictureUrl, location, start, end) {
    return `
    <div class="col h-auto">
        <div class="card shadow-lg p-0 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <p>${start} - ${end}</p>
            </div>
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error("No response");
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    // const details = await detailResponse.json();
                    // const nameTag = document.querySelector('.card-title')
                    // nameTag.innerHTML = details.conference.name;
                    // const descTag = document.querySelector('.card-text')
                    // descTag.innerHTML = details.conference.description;
                    // const imageTag = document.querySelector('.card-img-top')
                    // imageTag.src = details.conference.location.picture_url;
                    // console.log(details);
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const start = new Date(details.conference.starts).toLocaleDateString()
                    const end = new Date(details.conference.ends).toLocaleDateString()
                    const html = createCard(name, description, pictureUrl, location, start, end);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    } catch (e) {
        console.error('error', e)
    }

});





// function createCard(name, description, pictureUrl, location) {
//     return `
//     <div class="card">
//         <img src="${pictureUrl}" class="card-img-top">
//         <div class="card-body">
//             <h5 class="card-title">${name}</h5>
//             <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
//             <p class="card-text">${description}</p>
//         </div>
//     </div>
// `;
// }





// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//             console.error("someting went wrong boyos")
//         } else {
//             const data = await response.json();

//             for (let conference of data.conferences) {
//                 const detailUrl = `http://localhost:8000${conference.href}`;
//                 const detailResponse = await fetch(detailUrl);
//                 if (detailResponse.ok) {
//                     // const details = await detailResponse.json();
//                     // const nameTag = document.querySelector('.card-title')
//                     // nameTag.innerHTML = details.conference.name;
//                     // const descTag = document.querySelector('.card-text')
//                     // descTag.innerHTML = details.conference.description;
//                     // const imageTag = document.querySelector('.card-img-top')
//                     // imageTag.src = details.conference.location.picture_url;
//                     // console.log(details);

//                     const details = await detailResponse.json();
//                     const name = details.conference.name;
//                     const description = details.conference.description;
//                     const pictureUrl = details.conference.location.picture_url;
//                     const column = document.querySelector('.col');
//                     const html = createCard(name, description, pictureUrl);
//                     column.innerHTML += html;


//                 }
//             }

//         }
//     } catch (e) {
//         console.error("bruh moment", e)
//     }

// });
